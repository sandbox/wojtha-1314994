<?php

/**
 * Form for editing an entire menu tree at once.
 *
 * Shows for one menu the menu links accessible to the current user and
 * relevant operations.
 */
function submenutree_adv_overview_form($form, &$form_state, $node) {
  global $menu_admin;

  $mlid = submenutree_get_mlid($node);

  if ($mlid) {
    // TODO: Don't return $my_tree or $parent_tree if $node->submenutree_enable
    // or $node->siblingmenutree_enable flags are off.
    list($my_tree, $parent_tree) = submenutree_get_menu_tree($mlid);

    // Sanity check that we did find something
    if ($my_tree && !empty($node->submenutree_enable)) {
      $form['submenutree'] = array(
        '#tree' => TRUE,
      );
      $form['submenutree'] = array_merge($form, _submenutree_adv_overview_tree_form('submenutree', $my_tree));
    }
    if ($parent_tree && !empty($node->siblingmenutree_enable)) {
      $form['siblingmenutree'] = array(
        '#tree' => TRUE,
      );
      $form['siblingmenutree'] = array_merge($form, _submenutree_adv_overview_tree_form('siblingmenutree', $parent_tree));
    }
  }

  $form['#node'] =  $node;

  if (element_children($form)) {
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    );
  }
  else {
    $form['#empty_text'] = t('There are no menu links yet.');
  }
  return $form;
}

/**
 * Recursive helper function for submenutree_adv_overview_form().
 *
 * @param $tree
 *   The menu_tree retrieved by menu_tree_data.
 */
function _submenutree_adv_overview_tree_form($type, $tree) {
  $form = &drupal_static(__FUNCTION__ . $type, array('#tree' => TRUE));
  $display_options = _submenutree_adv_display_options();
  foreach ($tree as $data) {
    $title = '';
    $item = $data['link'];

    // Get display from display table
    $item['display'] = submenutree_adv_item_display_load($item['mlid']);

    // Don't show callbacks; these have $item['hidden'] < 0.
    if ($item && $item['hidden'] >= 0) {
      $mlid = 'mlid:' . $item['mlid'];
      $form[$mlid]['#item'] = $item;
      $form[$mlid]['#attributes'] = $item['hidden'] ? array('class' => array('menu-disabled')) : array('class' => array('menu-enabled'));
      $form[$mlid]['title']['#markup'] = l($item['title'], $item['href'], $item['localized_options']) . ($item['hidden'] ? ' (' . t('disabled') . ')' : '');
      $form[$mlid]['hidden'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable @title menu link', array('@title' => $item['title'])),
        '#title_display' => 'invisible',
        '#default_value' => !$item['hidden'],
      );
      $form[$mlid]['display'] = array(
        '#type' => 'select',
        '#options' => $display_options,
        '#title' => t('Display of the @title menu item', array('@title' => $item['title'])),
        '#title_display' => 'invisible',
        '#default_value' => isset($item['display']) ?  $item['display'] : '',
      );
      $form[$mlid]['weight'] = array(
        '#type' => 'weight',
        '#delta' => 50,
        '#default_value' => $item['weight'],
        '#title_display' => 'invisible',
        '#title' => t('Weight for @title', array('@title' => $item['title'])),
      );
      $form[$mlid]['mlid'] = array(
        '#type' => 'hidden',
        '#value' => $item['mlid'],
      );
      $form[$mlid]['plid'] = array(
        '#type' => 'hidden',
        '#default_value' => $item['plid'],
      );
      // Build a list of operations.
      $operations = array();
      $operations['edit'] = array('#type' => 'link', '#title' => t('edit'), '#href' => 'admin/structure/menu/item/' . $item['mlid'] . '/edit');
      // Only items created by the menu module can be deleted.
      if ($item['module'] == 'menu' || $item['updated'] == 1) {
        $operations['delete'] = array('#type' => 'link', '#title' => t('delete'), '#href' => 'admin/structure/menu/item/' . $item['mlid'] . '/delete');
      }
      // Set the reset column.
      elseif ($item['module'] == 'system' && $item['customized']) {
        $operations['reset'] = array('#type' => 'link', '#title' => t('reset'), '#href' => 'admin/structure/menu/item/' . $item['mlid'] . '/reset');
      }
      $form[$mlid]['operations'] = $operations;
    }
  }
  return $form;
}

/**
 * Submit handler for the menu overview form.
 *
 * This function takes great care in saving parent items first, then items
 * underneath them. Saving items in the incorrect order can break the menu tree.
 *
 * @see submenutree_adv_overview_form()
 */
function submenutree_adv_overview_form_submit($form, &$form_state) {
  // When dealing with saving menu items, the order in which these items are
  // saved is critical. If a changed child item is saved before its parent,
  // the child item could be saved with an invalid path past its immediate
  // parent. To prevent this, save items in the form in the same order they
  // are sent by $_POST, ensuring parents are saved first, then their children.
  // See http://drupal.org/node/181126#comment-632270
  $order = array_flip(array_keys($form_state['input'])); // Get the $_POST order.
  $form = array_merge($order, $form); // Update our original form with the new order.

  $updated_items = array();
  $fields = array('weight', 'display');
  foreach (array('submenutree', 'siblingmenutree') as $type) {
    if (isset($form[$type])) {
      foreach (element_children($form[$type]) as $mlid) {
        if (isset($form[$type][$mlid]['#item'])) {
          $element = $form[$type][$mlid];
          // Update any fields that have changed in this menu item.
          foreach ($fields as $field) {
            if ($element[$field]['#value'] != $element[$field]['#default_value']) {
              $element['#item'][$field] = $element[$field]['#value'];
              $updated_items[$mlid] = $element['#item'];
            }
          }
          // Hidden is a special case, the value needs to be reversed.
          if ($element['hidden']['#value'] != $element['hidden']['#default_value']) {
            // Convert to integer rather than boolean due to PDO cast to string.
            $element['#item']['hidden'] = $element['hidden']['#value'] ? 0 : 1;
            $updated_items[$mlid] = $element['#item'];
          }
        }
      }
    }
  }

  // Save all our changed items to the database.
  foreach ($updated_items as $item) {
    $item['customized'] = 1;
    submenutree_adv_item_display_save($item['mlid'], $item['display']);
    menu_link_save($item);
  }
  drupal_set_message(t('Your configuration has been saved.'));
}

/**
 * Returns HTML for the menu overview form into a table.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
function theme_submenutree_adv_overview_form($variables) {
  $form = $variables['form'];
  $output = '';

  $output .= '<h3>' . t('Submenu Tree') . '</h3>';
  if (!empty($form['#node']->submenutree_enable)) {
    $output .= _submenutree_adv_overview_table($form['submenutree'], 'submenutree-overview');
  }
  else {
    $output .= '<p>' . t('Submenutree is disabled for this node') . '</p>';
  }

  $output .= '<h3>' . t('Siblingmenu Tree') . '</h3>';
  if (!empty($form['#node']->siblingmenutree_enable)) {
    $output .= _submenutree_adv_overview_table($form['siblingmenutree'], 'siblingmenutree-overview');
  }
  else {
    $output .= '<p>' . t('Siblingmenutree is disa bled for this node') . '</p>';
  }

  $output .= drupal_render_children($form);

  return $output;
}

function _submenutree_adv_overview_table(&$form, $table_id) {

  drupal_add_tabledrag($table_id, 'order', 'sibling', 'menu-weight');

  $header = array(
    t('Menu link'),
    array('data' => t('Enabled'), 'class' => array('checkbox')),
    t('Display'),
    t('Weight'),
    array('data' => t('Operations'), 'colspan' => '3'),
  );

  $rows = array();
  foreach (element_children($form) as $mlid) {
    if (isset($form[$mlid]['hidden'])) {
      $element = &$form[$mlid];
      // Build a list of operations.
      $operations = array();
      foreach (element_children($element['operations']) as $op) {
        $operations[] = array('data' => drupal_render($element['operations'][$op]), 'class' => array('menu-operations'));
      }
      while (count($operations) < 2) {
        $operations[] = '';
      }

      // Add special classes to be used for tabledrag.js.
      $element['plid']['#attributes']['class'] = array('menu-plid');
      $element['mlid']['#attributes']['class'] = array('menu-mlid');
      $element['weight']['#attributes']['class'] = array('menu-weight');

      // Change the parent field to a hidden. This allows any value but hides the field.
      $element['plid']['#type'] = 'hidden';

      $row = array();
      $row[] = drupal_render($element['title']);
      $row[] = array('data' => drupal_render($element['hidden']), 'class' => array('checkbox', 'menu-enabled'));
      $row[] = drupal_render($element['display']);
      $row[] = drupal_render($element['weight']) . drupal_render($element['plid']) . drupal_render($element['mlid']);
      $row = array_merge($row, $operations);

      $row = array_merge(array('data' => $row), $element['#attributes']);
      $row['class'][] = 'draggable';
      $rows[] = $row;
    }
  }
  $output = '';
  if (empty($rows)) {
    $rows[] = array(array('data' => $form['#empty_text'], 'colspan' => '7'));
  }
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => $table_id)));

  return $output;
}
